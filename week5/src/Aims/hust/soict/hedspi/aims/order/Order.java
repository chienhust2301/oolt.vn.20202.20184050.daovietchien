package Aims.hust.soict.hedspi.aims.order;

import Aims.hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import Aims.hust.soict.hedspi.aims.utils.MyDate;

public class Order {
    public static final int MAX_NUMBERS_ORDERED =10;
    private int qtyOrdered;
    private DigitalVideoDisc itemsOrdered[] = new
            DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    private MyDate dateOrdered  = new MyDate();
    public static final int MAX_LIMITTED_ORERS = 5;
    private static int nbOrders = 0;
    public Order(){
        if(this.nbOrders < MAX_NUMBERS_ORDERED) {
            this.nbOrders++;
            System.out.println("Succes , total ordered "+ this.nbOrders);

        }else {
            System.out.println("Failed");
        }


    }
    public void print(Order item){
        DigitalVideoDisc itemsOrdered[] = item.itemsOrdered;
        for(int i=0;i< item.qtyOrdered;i++){
            System.out.println("DVD "+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+"-"
                    +itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+totalCost());

        }
    }
    public void print(){
        DigitalVideoDisc itemsOrdered[] = this.itemsOrdered;
        for(int i=0;i< this.qtyOrdered;i++){
            System.out.println("DVD "+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+"-"
                    +itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+totalCost());

        }
    }
    public DigitalVideoDisc getALuckyItem(){

        int free = (int)(Math.random() * (qtyOrdered - 0 + 1) + 0);
        DigitalVideoDisc itemfree = itemsOrdered[free];
        removeDigitalVideoDisc(itemfree);
        float totalcost = totalCost();
        System.out.println("Total cost : " +totalcost);
        print();
        System.out.println("Item free: " + itemfree.getTitle());

        return itemfree;



    }


    public MyDate getDateOrdered() {
        return dateOrdered;
    }


    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
    public void addDigitalVideoDisc(DigitalVideoDisc disc){
        if(qtyOrdered < MAX_NUMBERS_ORDERED){
            itemsOrdered[qtyOrdered++] = disc;
            System.out.println("Success");


        }else {
            System.out.println("Failed");
        }

    }
    public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2){
        this.addDigitalVideoDisc(dvd1);
        this.addDigitalVideoDisc(dvd2);

    }
    public void addDigitalVideoDisc(DigitalVideoDisc []dvlist){
        int n = dvlist.length;
        if(qtyOrdered+ dvlist.length<MAX_NUMBERS_ORDERED){
            for (int i=0;i<n;i++){
                this.addDigitalVideoDisc(dvlist[i]);
                System.out.println("Success");
            }

        }else {
            System.out.println("Failed");
        }
    }
    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
        String tmp = disc.getTitle();
        if(qtyOrdered == 0){
            System.out.println("Failed remove");
        }else{
            for(int i=0; i<qtyOrdered; i++){
                if(itemsOrdered[i].getTitle().equals(tmp)){
                    for(int j = i; j < qtyOrdered - 1; j++)
                    {
                        itemsOrdered[j] = itemsOrdered[j+1];
                    }
                    itemsOrdered[qtyOrdered - 1] = null;
                    qtyOrdered--;

                }
            }

        }


    }
    public float totalCost(){
        float total_cost = 0.0f;
        for (int i = 0; i < qtyOrdered; i++) {
            total_cost += itemsOrdered[i].getCost();
        }
        return total_cost;

    }
}
