package Aims.hust.soict.hedspi.aims.utils;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class MyDate {
    private int day;
    private int month;
    private int year;

    // Hàm khởi tạo không tham số
    public MyDate() {

        // Lấy ngày tháng năm hiện tại
        Calendar cal = Calendar.getInstance();
        setDay(cal.get(Calendar.DAY_OF_MONTH));

        // Chú ý: Month trong Java đếm từ 0
        setMonth(cal.get(Calendar.MONTH) + 1);
        setYear(cal.get(Calendar.YEAR));

    }

    // Hàm khởi tạo có 3 tham số ngày, tháng, năm
    public MyDate(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public void nhap() {
        String strNgay, strThang, strNam;
        int iNgay, iThang, iNam;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Nhap ngay: ");
            strNgay = sc.nextLine();
            iNgay = setNgay(strNgay);
        } while (iNgay < 1 || iNgay > 32);

        do {
            System.out.println("Nhap thang: ");
            strThang = sc.nextLine();
            iThang = setThang(strThang);
        } while (iThang < 1 || iThang > 12);

        do {
            System.out.println("Nhap nam: ");
            strNam = sc.nextLine();
            iNam = setYear(strNam);
        } while (iNam < 0);

        setDay(iNgay);
        setMonth(iThang);
        setYear(iNam);
    }

    public void hienThi() {
        Calendar cal = Calendar.getInstance();
        cal.set(getYear(), getMonth() - 1, getDay());

        System.out.println(((cal.get(Calendar.DAY_OF_WEEK) == 1 ? "Chu nhat" : "Thu" + cal.get(Calendar.DAY_OF_WEEK)))
                + ", ngay: " + getDay() + "/" + getMonth() + "/" + getYear());
    }

    public void hienThiKieuString() {
        Calendar cal = Calendar.getInstance();
        cal.set(getYear(), getMonth() - 1, getDay());
        String month = new DateFormatSymbols().getMonths()[getMonth() - 1];

        String day = "NULL";
        String[] dayOfMonth = new String[]{"first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thiry-first"};
        for (int i = 0; i < dayOfMonth.length; i++) {
            int j = i + 1;
            if (j == getDay()) {
                day = dayOfMonth[i];
                break;
            }
        }
        System.out.println(day + " " + month + " " + getYear());
    }

    public static void hienThiNgayHienTai() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        String strDate = formatter.format(date);
        System.out.println("Dinh dang MM/dd/yyyy: " + strDate);

        formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        strDate = formatter.format(date);
        System.out.println("Dinh dang dd-M-yyyy hh:mm:ss: " + strDate);

        formatter = new SimpleDateFormat("dd MMMM yyyy");
        strDate = formatter.format(date);
        System.out.println("Dinh dang dd MMMM yyyy: " + strDate);

        formatter = new SimpleDateFormat("dd MMMM yyyy zzzz");
        strDate = formatter.format(date);
        System.out.println("Dinhg dang dd MMMM yyyy zzzz: " + strDate);

        formatter = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        strDate = formatter.format(date);
        System.out.println("Dinh dang E, dd MMM yyyy HH:mm:ss z: " + strDate);

    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int setNgay(String ngay) {
        String[] dayOfMonth = new String[]{"first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second", "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth", "twenty-ninth", "thirtieth", "thiry-first"};
        for (int i = 0; i < dayOfMonth.length; i++)
            if (ngay.equalsIgnoreCase(dayOfMonth[i])) {
                return this.day = i + 1;
            }
        return 0;
    }

    public int getMonth() {
        return month;
    }

    public int setThang(String thang) {
        String[] monthOfYear = new String[]{"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
        for (int i = 0; i < monthOfYear.length; i++) {
            if (thang.equalsIgnoreCase(monthOfYear[i])) {
                return this.day = i + 1;
            }
        }
        return 0;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public int setYear(String year) {
        switch (year) {
            case "twenty twenty":
                return this.year = 2020;
            case "twenty nineteen":
                return this.year = 2019;
            case "twenty eighteen":
                return this.year = 2018;
            case "twenty seventeen":
                return this.year = 2017;
            default:
                break;
        }
        return 0;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
