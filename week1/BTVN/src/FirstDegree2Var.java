
import javax.swing.JOptionPane;
public class FirstDegree2Var {

    public static void main(String[] args) {
        Double a11, a12, b1, a21, a22, b2, D, D1, D2;

        a11 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the a11 number",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));
        a12 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the a12 number",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));
        b1 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the b1 number",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));


        a21 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the a21 number",
                "Input the second name",
                JOptionPane.INFORMATION_MESSAGE));
        a22 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the a22 number",
                "Input the second name",
                JOptionPane.INFORMATION_MESSAGE));
        b2 = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the b2 number",
                "Input the second name",
                JOptionPane.INFORMATION_MESSAGE));

        D = a11 * a22 - a21 * a12;
        D1 = b1 * a22 - b2 * a12;
        D2 = a11 * b2 - a21 * b1;

        if (D != 0) {

            JOptionPane.showMessageDialog(null,"Result x1: " + D1/D+ " Result x2: "
            +D2/D);

        } else if (D == 0 && D1 == 0 && D2 == 0) {

            JOptionPane.showMessageDialog(null,"Infinitely many solutions. ");
        } else {

            JOptionPane.showMessageDialog(null,"No solution.");
        }
    }
}