import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * SecondDegree1Var
 */
public class SecondDegree1Var {

    public static void main(String[] args) {
        Double a, b, c, delta;

        a =  Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the a number (a!=0)",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));

        b =  Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the b number",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));

        c =  Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please input the c number",
                "Input the  name",
                JOptionPane.INFORMATION_MESSAGE));

        delta = b * b - 4 * a * c;

        if (delta == 0) {

            JOptionPane.showMessageDialog(null,"Result: x1 = x2 = "+-b/(2*a));

        } else if (delta > 0) {
   
            JOptionPane.showMessageDialog(null,"Result: x1 = " +
                    (-b + Math.sqrt(delta)) / (2 * a) +
                " x2 = " +(-b - Math.sqrt(delta)) / (2 * a));

        } else {

            JOptionPane.showMessageDialog(null,"No solution.");
        }
    }
}