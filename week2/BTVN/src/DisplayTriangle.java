import java.util.Scanner;
public class DisplayTriangle {
    public static void main(String[] args) {
        Scanner Keyboard = new Scanner(System.in);
        System.out.println("Input the n: ");
        int n = Keyboard.nextInt();
        if (n<0){
            System.out.println("Input the n: ");
            n = Keyboard.nextInt();
        }else{
            for (int i=0;i<n;i++){
                for(int j=0;j<n-i-1;j++){
                    System.out.print(" ");
                }
                for(int j=0;j<2*i+1;j++){
                    System.out.print("*");
                }
                System.out.print("\n");

            }
        }
    }
}
