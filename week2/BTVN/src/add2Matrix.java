import java.util.Arrays;
import java.util.Scanner;
public class add2Matrix {
    public static void main(String[] args) {
        int m,n;
        Scanner Input = new Scanner(System.in);
        System.out.println("Input m");

        m=Input.nextInt();
        System.out.println("Input n");
        n=Input.nextInt();

        int array1[][] = new int[m][n];
        int array2[][] = new int[m][n];
        int sum[][] = new int[m][n];
        System.out.println("Input first matrix");
        for(int i=0;i<m;i++){
            for (int j=0;j<n;j++){
                array1[i][j]=Input.nextInt();
            }
        }
        System.out.println("Input second matrix");
        for(int i=0;i<m;i++){
            for (int j=0;j<n;j++){
                array2[i][j]=Input.nextInt();
            }
        }
        System.out.println("Sum of matrix");
        for(int i=0;i<m;i++){
            for (int j=0;j<n;j++){
                sum[i][j]= array1[i][j] +  array2[i][j];
            }
        }
        for ( int i = 0 ; i < m ; i++ )
        {
            for (int  j = 0 ; j < n ; j++ )
                System.out.print(sum[i][j]+"\t");

            System.out.println();
        }

    }
}
