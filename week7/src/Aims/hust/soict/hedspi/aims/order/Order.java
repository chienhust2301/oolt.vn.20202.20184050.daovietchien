package Aims.hust.soict.hedspi.aims.order;

import Aims.hust.soict.hedspi.aims.media.DigitalVideoDisc;
import Aims.hust.soict.hedspi.aims.media.Media;
import Aims.hust.soict.hedspi.aims.utils.MyDate;

import java.util.ArrayList;

public class Order {

    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();


    public Order(){
        super();
    }
    public void addMedia(Media media){
        if (!(itemsOrdered.contains(media))) {
            itemsOrdered.add(media);
        }
    }

    public void removeMedia(Media media) {
        if (itemsOrdered.contains(media)) {
            itemsOrdered.remove(media);
        }
    }
    public void removeMedia(int id) {
        for (int i = 0; i < itemsOrdered.size(); i++) {
            if (itemsOrdered.get(i).getId() == id) {
                itemsOrdered.get(i).setId(0);
                itemsOrdered.get(i).setCost(0);
                itemsOrdered.get(i).setTitle(null);
                itemsOrdered.get(i).setCategory(null);
            }
        }
    }
    public void getItemOrder() {
        for (int i = 0; i < itemsOrdered.size(); i++) {
            System.out.println("\nID: " + itemsOrdered.get(i).getId());
            System.out.println("Title: " + itemsOrdered.get(i).getId());
            System.out.println("Category: " + itemsOrdered.get(i).getCategory());
            System.out.println("Cost: " + itemsOrdered.get(i).getCost());
        }
    }



    public float totalCost(){
      float total = 0;
      Media mediaItem;
      java.util.Iterator iter = itemsOrdered.iterator();
      while (iter.hasNext()){
          mediaItem = (Media)(iter.next());
          total += mediaItem.getCost();
      }
      return total;

    }
}
