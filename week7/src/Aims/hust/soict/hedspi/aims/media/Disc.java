package Aims.hust.soict.hedspi.aims.media;

public class Disc extends Media{
    protected int length;
    protected String director;



    public Disc(){
        super();
    }
    public Disc(String title, String category, float cost,int id, int length, String director){
        super(title, category, cost, id);
        this.length = length;
        this.director = director;
    }
    public Disc(String title, String category, float cost, int id) {
        super(title,category,cost,id);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
}
