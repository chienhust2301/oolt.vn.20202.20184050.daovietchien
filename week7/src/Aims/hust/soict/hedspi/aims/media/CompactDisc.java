package Aims.hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable {
    private ArrayList<Track> tracks = new ArrayList <Track>();
    private String artist;

    public void setArtist(String artist) {
        this.artist = artist;
    }
    public CompactDisc(){

    }
    public CompactDisc(String title, String category, float cost, int id){
        super(title, category, cost, id);
    }

    public String getArtist() {
        return artist;
    }
    public void addTrack(Track track){
        if(!this.tracks.contains(track)){
            this.tracks.add(track);
        }
    }
    public void removeTrack(Track track){
        if(this.tracks.contains(track)){
            this.tracks.remove(track);
        }
    }
    public int getLength(){

        int sum = 0;
        for (Track tracktemp : this.tracks){
            sum = sum + tracktemp.getLength();
        }
        return sum;

    }
    public void play() {
        for (Track track : tracks) {
            System.out.println("Track title: " + track.getTitle()
                    + " - Track lenght: " + track.getLength());
        }


        System.out.println("Length: " + getLength());
    }

}
