package ex2;

import javax.swing.*;
import java.util.Calendar;

public class MyDate {
    private int day;
    private int month;
    private int year;
    public MyDate() {


        Calendar cal = Calendar.getInstance();
        setDay(cal.get(Calendar.DAY_OF_MONTH));

        setMonth(cal.get(Calendar.MONTH) + 1);
        setYear(cal.get(Calendar.YEAR));

    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    public MyDate(String str) {
        if (str.matches("\\d{2}/\\d{2}/\\d{4}")) {
            String[] s = str.split("/");
            this.day = Integer.parseInt(s[0]);
            this.month = Integer.parseInt(s[1]);
            this.year = Integer.parseInt(s[2]);
        }
    }
    public void accept(){
        String strNgay, strThang, strNam;
        int day, month, year;

        do {
            strNgay = JOptionPane.showInputDialog(null, "Input day: ");
            day = Integer.parseInt(strNgay);
        } while (day < 1 || day > 32);

        do {
            strThang = JOptionPane.showInputDialog(null, "Input month: ");
            month = Integer.parseInt(strThang);
        } while (month < 1 || month > 12);

        do {
            strNam = JOptionPane.showInputDialog(null, "Input year: ");
            year = Integer.parseInt(strNam);
        } while (year < 0);

        setDay(day);
        setMonth(month);
        setYear(year);
    }
    public void print() {
        System.out.printf("%02d/%02d/%04d\n", this.day, this.month, this.year);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
