package ex2;

public class TestMyData {
    public static void main(String[] args) {
        MyDate obj1 = new MyDate();
        obj1.print();

        MyDate obj2 = new MyDate(20, 3, 2021);
        obj2.print();

        MyDate obj3 = new MyDate();
        obj3.accept();
        obj3.print();
    }

}
